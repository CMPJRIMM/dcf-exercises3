package uk.ac.ljmu.fet.cs.cmpjrimm.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ConstantConstraints;
import hu.unimiskolc.iit.distsys.BuiltInCloudProvider;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;
import uk.ac.ljmu.fet.cs.cmpjrimm.MinimumProvider;

public class MinimalProviderTest {

	/**
	 * Testing for the tick quote is smaller than the built in provider
	 */
	@Test
	public void testPriceSmaller() {
		CloudProvider minimum = new MinimumProvider();
		CloudProvider basic = new BuiltInCloudProvider();
		double minPrice = minimum.getPerTickQuote(ConstantConstraints.noResources);
		
		double builtPrice = basic.getPerTickQuote(ConstantConstraints.noResources);
		assertTrue("Price should be lower than built in", minPrice < builtPrice);
		
				
		
		
	}
	
	@Test
	public void testNoFreePrice() {
		CloudProvider minimum = new MinimumProvider();

		assertTrue("Per Tick Quote should be postive.", minimum.getPerTickQuote(ConstantConstraints.noResources)>0);
		
		
	}


}
