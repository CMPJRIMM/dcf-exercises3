package uk.ac.ljmu.fet.cs.cmpgkecs;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.unimiskolc.iit.distsys.ExercisesBase;

public class SecondDCFCode {
	public static void logMessage(String message) {
		System.out.println("@ T+" + Timed.getFireCount() + "ms " + message);
	}

	public static void main(String[] args) throws Exception {
		// A cloud with 2 computers in it
		IaaSService theCloud = ExercisesBase.getComplexInfrastructure(2);
		// We set up a webserver on our cloud:
		WebServer ourLampServer = new WebServer(theCloud);
		// We create our laptop:
		BrowserOnLaptop ourBrowser = new BrowserOnLaptop();
		// Then we ask for our laptop to check what the server is offering:
		ourBrowser.visualiseAWebPage(ourLampServer);

		// Let's actually do everything:
		Timed.simulateUntilLastEvent();
	}
}
