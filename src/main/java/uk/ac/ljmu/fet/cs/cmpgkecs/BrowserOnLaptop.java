package uk.ac.ljmu.fet.cs.cmpgkecs;

import hu.mta.sztaki.lpds.cloud.simulator.DeferredEvent;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.resourcemodel.ResourceConsumption;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.resourcemodel.ResourceConsumption.ConsumptionEvent;
import hu.mta.sztaki.lpds.cloud.simulator.io.NetworkNode;
import hu.mta.sztaki.lpds.cloud.simulator.io.NetworkNode.NetworkException;
import hu.unimiskolc.iit.distsys.ExercisesBase;

public class BrowserOnLaptop implements ConsumptionEvent {
	PhysicalMachine laptop;

	public BrowserOnLaptop() throws Exception {
		laptop = ExercisesBase.getNewPhysicalMachine();
	}

	public NetworkNode getNIC() {
		return laptop.localDisk;
	}

	public void visualiseAWebPage(final WebServer fromTheServer) {
		if (fromTheServer.getNIC() == null) {
			SecondDCFCode.logMessage("Server is not responding, let's wait!");
			// The webserver is not yet ready, let's wait a minute.
			new DeferredEvent(60 * 1000) {
				@Override
				protected void eventAction() {
					visualiseAWebPage(fromTheServer);
				}
			};
		} else {
			// Here we can communicate to the LAMP VM of ours by starting a
			// typical HTTP request message:
			// http://stackoverflow.com/questions/5358109/what-is-the-average-size-of-an-http-request-response-header
			try {
				SecondDCFCode.logMessage("Finally we are sending our request!");
				fromTheServer.registerForFeedback(this);
				NetworkNode.initTransfer(800, ResourceConsumption.unlimitedProcessing, getNIC(), fromTheServer.getNIC(),
						fromTheServer);
			} catch (NetworkException someBadNews) {
				System.err.println("This is not really happening!?!");
				System.exit(3);
			}
		}
	}

	@Override
	public void conComplete() {
		SecondDCFCode.logMessage("The webpage has arrived!");
	}

	@Override
	public void conCancelled(ResourceConsumption problematic) {

	}
}
