
package uk.ac.ljmu.fet.cs.cmpjrimm;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService.IaaSHandlingException;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;



public class MyFirstProvider implements CloudProvider {

	
	@Override
	public double getPerTickQuote(ResourceConstraints rc) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setIaaSService(IaaSService iaas) {
		int machineNo = iaas.machines.size();
		System.out.println(machineNo);
		int halfNo = machineNo / 2;
		
		for(int x = 0; x < halfNo; x++) {
			PhysicalMachine pm = iaas.machines.get(x);
			try {
				iaas.deregisterHost(pm);
				
			} catch (IaaSHandlingException e) {
				e.printStackTrace();
			}
		}
	}
	
}




