package uk.ac.ljmu.fet.cs.cmpjrimm;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ConstantConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.BuiltInCloudProvider;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;

public class MinimumProvider implements CloudProvider {
	private static CloudProvider BuiltIn = new BuiltInCloudProvider();
	
	@Override
	public double getPerTickQuote(ResourceConstraints rc) {
		// TODO Auto-generated method stub
		return (BuiltIn.getPerTickQuote(ConstantConstraints.noResources) * 0.75);
	}

	@Override
	public void setIaaSService(IaaSService iaas) {
		// TODO Auto-generated method stub

	}

}
