package uk.ac.ljmu.fet.cs.cmpjrimm;

import hu.mta.sztaki.lpds.cloud.simulator.DeferredEvent;

public class WriteOnce extends DeferredEvent {
	private String payload;
	PeriodicWriter periodic;
	
	public WriteOnce(PeriodicWriter p) {
		super(2);
		periodic = p;
		
	}
	
	public void setPayload(String payload) {
		this.payload = payload;
	}

	@Override
	protected void eventAction() {
		System.out.print(payload + " from DISSECT-CF");
		periodic.setMarker("!");

	}

	public static void setMarker(String string) {
		// TODO Auto-generated method stub
		
	}

}
